<?php

$search_words = ['php','html','интернет','Web'];

$search_strings = [
'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',

'PHP - это распространенный язык программирования с открытым исходным кодом.',

'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'];

$regExpForWords1 = '/^интернет$/iu';
$regExpForWords2 = '/^php$/i';
$regExpForWords3 = '/^web$/i';

$regExpForString1 = '/^интернет - .*, ?.+\.$/iu';
$regExpForString2 = '/^php - .*\.$/iu';
$regExpForString3 = '/^php.+ web.+ html$/iu';

echo '<pre>';
echo '<h3>1-й вариант, попытка исполнения, используя $search_words.</h3>';

foreach ($search_words as $search_word){
    if (preg_match($regExpForWords1,$search_word))
    {
        foreach ($search_strings as $search_string)
        {
            if (preg_match($regExpForString1,$search_string))
            {
                echo "в строке №1 есть слова: $search_word";
                echo '<br>';
            }
        }

    }elseif (preg_match($regExpForWords2,$search_word))
    {
        foreach ($search_strings as $search_string)
        {
            if (preg_match($regExpForString2,$search_string))
            {
                echo "в строке №2 есть слова: $search_word";
                echo '<br>';
            }
        }
    }elseif (preg_match($regExpForWords3,$search_word))
    {
        foreach ($search_strings as $search_string)
        {
            if (preg_match($regExpForString3,$search_string))
            {
                echo "в строке №3 есть слова:php, ".$search_word.', html';
                echo '<br>';
            }
        }
    }
}
echo '<pre>';
echo '<h3>2-й вариант - красиво, строки упорядочены, но $search_words не используется.</h3>';

foreach ($search_strings as $search_string){
    if (preg_match($regExpForString1,$search_string))
    {
        echo "в строке №1 есть слова: интернет";
        echo '<br>';
    }elseif (preg_match($regExpForString2,$search_string))
    {
        echo "в строке №2 есть слова: php";
        echo '<br>';
    }elseif (preg_match($regExpForString3,$search_string))
    {
        echo "в строке №3 есть слова: php, web, html";
        echo '<br>';
    }

}

